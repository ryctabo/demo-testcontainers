package com.ryctabo.demo.testcontainers

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ApplicationTest: AbstractIntegrationTest() {

    @Test
    fun contextLoads() {
    }
}
