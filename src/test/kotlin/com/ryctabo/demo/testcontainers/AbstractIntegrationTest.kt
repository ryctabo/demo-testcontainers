package com.ryctabo.demo.testcontainers

import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.utility.DockerImageName

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner::class)
@ContextConfiguration(initializers = [AbstractIntegrationTest.Initializer::class])
abstract class AbstractIntegrationTest {
    companion object {
        val mongoDbContainer = MongoDBContainer(DockerImageName.parse("mongo:4.4.17"))
    }

    internal class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

        override fun initialize(applicationContext: ConfigurableApplicationContext) {
            mongoDbContainer.start()

            TestPropertyValues.of(
                "spring.data.mongodb.host=${mongoDbContainer.host}",
                "spring.data.mongodb.port=${mongoDbContainer.firstMappedPort}"
            ).applyTo(applicationContext.environment)
        }
    }
}