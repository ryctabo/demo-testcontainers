package com.ryctabo.demo.testcontainers.http.resources

import com.ryctabo.demo.testcontainers.AbstractIntegrationTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

internal class HelloControllerTest : AbstractIntegrationTest() {

    @Autowired
    private lateinit var mockMvc: MockMvc;

    @Test
    fun `given a request to get without name param must be respond 'Hello world!'`() {
        mockMvc.perform(get("/hello"))
            .andExpect(status().isOk)
            .andExpect(content().string("Hello world!"))
    }

    @Test
    fun `given a request to get with name param must be respond 'Hi Gustavo!'`() {
        mockMvc.perform(get("/hello").queryParam("name", "Gustavo"))
            .andExpect(status().isOk)
            .andExpect(content().string("Hi Gustavo!"))
    }
}