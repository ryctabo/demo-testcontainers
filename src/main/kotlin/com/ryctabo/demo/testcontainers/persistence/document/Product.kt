package com.ryctabo.demo.testcontainers.persistence.document

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.Instant

@Document(collection = "products")
data class Product(
    @Id val id: String?,
    val name: String,
    val description: String?,
    val price: BigDecimal,
    val createdAt: Instant
)