package com.ryctabo.demo.testcontainers.persistence.repository

import com.ryctabo.demo.testcontainers.persistence.document.Product
import org.springframework.data.mongodb.repository.MongoRepository

interface MongoProductRepository: MongoRepository<Product, String>