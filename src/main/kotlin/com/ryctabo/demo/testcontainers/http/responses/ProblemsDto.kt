package com.ryctabo.demo.testcontainers.http.responses

data class Problem(
    val title: String,
    val description: String?,
    val ref: String?,
    val status: Int,
    val path: String,
    val code: String?,
    val invalidParams: List<ConstraintViolationResponse>? = null
)

data class ConstraintViolationResponse(
    val propertyName: String,
    val currentValue: String
)