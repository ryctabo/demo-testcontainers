package com.ryctabo.demo.testcontainers.http.resources

import com.ryctabo.demo.testcontainers.exceptions.ProductNotFoundException
import com.ryctabo.demo.testcontainers.persistence.document.Product
import com.ryctabo.demo.testcontainers.persistence.repository.MongoProductRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("products")
class ProductController(private val repository: MongoProductRepository) {

    @GetMapping
    fun get(): List<Product> {
        return repository.findAll();
    }

    @PostMapping
    fun post(@RequestBody request: Product): ResponseEntity<Product> {
        val product = repository.save(request)

        val location = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{productId}")
            .build(mapOf("productId" to product.id))

        return ResponseEntity.created(location).body(product)
    }

    @GetMapping("{id}")
    fun get(@PathVariable id: String): Product? {
        return repository.findById(id)
            .orElseThrow { ProductNotFoundException(id) }
    }
}