# Demo TestContainers

[![pipeline status](https://gitlab.com/ryctabo/demo-testcontainers/badges/main/pipeline.svg)](https://gitlab.com/ryctabo/demo-testcontainers/-/commits/main)
[![coverage report](https://gitlab.com/ryctabo/demo-testcontainers/badges/main/coverage.svg)](https://gitlab.com/ryctabo/demo-testcontainers/-/commits/main)
[![Latest Release](https://gitlab.com/ryctabo/demo-testcontainers/-/badges/release.svg)](https://gitlab.com/ryctabo/demo-testcontainers/-/releases)

Reference: https://www.testcontainers.org/
