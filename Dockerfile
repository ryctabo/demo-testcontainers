FROM gradle:7.5.1-jdk17-alpine as builder
WORKDIR /home/app
# copy all gradle configuration files
COPY build.gradle.kts .
COPY settings.gradle.kts .
COPY src ./src
# copy source files
RUN gradle assemble

FROM eclipse-temurin:17-jre-alpine
WORKDIR /opt/app
EXPOSE 8080
COPY --from=builder /home/app/build/libs/demo-testcontainers.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
